# -*- coding: utf-8 -*-
import numpy as np
import cv2, PIL
from cv2 import aruco
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import os
#%matplotlib nbagg


def get_objPoints(aruco_size=1, fig=None):
    '''
    Get object point for a given figure map
    '''
    def get_corners(bbox, aruco_size):

        sx = bbox.x1- bbox.x0
        sy = bbox.y1- bbox.y0
        square_size = sx
        if sx > sy:
            square_size = sy

        #import pdb; pdb.set_trace()
        p0 = [bbox.x0, bbox.y1, 0]
        p1 = [bbox.x0 + square_size, bbox.y1, 0]
        p2 = [bbox.x0 + square_size ,bbox.y1 - square_size, 0]
        p3 = [bbox.x0, bbox.y1 - square_size, 0]

        factor = aruco_size/square_size

        corners = np.asarray([p0,p1,p2,p3], dtype=np.float32)
        corners*= factor

        return corners

    objPoints = []
    for ax in fig.get_axes():
        bbox = ax.get_position()
        bbox.x0 *= fig.canvas.get_width_height()[0]
        bbox.x1 *= fig.canvas.get_width_height()[0]

        bbox.y0 *= fig.canvas.get_width_height()[1]
        bbox.y1 *= fig.canvas.get_width_height()[1]

        corners = get_corners(bbox, aruco_size)
        objPoints.append(corners)

    # get minimum point
    minPt = None
    for points in objPoints:
        pt = np.min(points, axis=0)
        if minPt is None:
            minPt = pt
        else:
            if minPt[0] >= pt[0] and minPt[1] >= pt[1]:
                minPt = pt
    for i, obj in enumerate(objPoints):
        objPoints[i] = (obj - minPt)

    return objPoints

def create_board_save(objPoints, aruco_dict, ids, filename):
    if type(ids) is list:
        ids = np.asarray(ids)

    board = aruco.Board_create(objPoints, aruco_dict, ids)
    img = PIL.Image.fromarray(aruco.drawPlanarBoard(
        board, fig.canvas.get_width_height()))
    img.save(filename)

def save_objMap(objPoints, ids, filename, arcuo_dict_name):

    stack = []; stack_ids=[]; stack_ind=[]
    for id_, objPoint in zip(ids, objPoints):
        for i, point in enumerate(objPoint):
            stack.append(point)
            stack_ids.append(id_)
            stack_ind.append(i)

    stack = np.asarray(stack)

    df = pd.DataFrame()
    df['id'] = stack_ids
    df['index'] = stack_ind
    df['x'] = stack[:,0]
    df['y'] = stack[:,1]
    df['z'] = stack[:,2]
    df['aruco_dict'] = [aruco_dict_name]*len(stack_ids)

    df.to_csv(filename + '.csv', sep=';')

def read_objMap(filename):

    df = pd.read_csv(filename, sep=';')

    objPoints = []
    ids = np.unique(df['id'].values)

    for id_ in ids:
        sub_df = df[df['id'].values == id_]
        indices = sorted(sub_df['index'].values)

        points = []
        for ind in indices:
            df_ = sub_df[sub_df['index'] == ind]
            point = df_[['x', 'y', 'z']].values[0]
            points.append(point)
        points = np.asarray(points, dtype= np.float32)
        objPoints.append(points)

    aruco_dict_name = np.unique(df['aruco_dict'].values)[0]
    aruco_dict = aruco.getPredefinedDictionary(aruco_dict_name)

    return objPoints, ids, aruco_dict

def create_board(filename):
    objPoints, ids, aruco_dict = read_objMap(filename)
    board = aruco.Board_create(objPoints, aruco_dict, ids)

    return board, aruco_dict

def save(aruco_size, fig, ids, filename, aruco_dict_name):
    aruco_dict = aruco.getPredefinedDictionary(aruco_dict_name)
    objPoints = get_objPoints(aruco_size, fig = fig)
    create_board_save(objPoints, aruco_dict, ids, filename)
    save_objMap(objPoints, ids, filename, aruco_dict_name)

    # for checking values dumped in file are correct
    if False:
        #checking if values are stored correctly
        r_objPoints, r_ids, r_aruco_dict = read_objMap(filename + '.csv')
        for points_r, points_a in zip(r_objPoints, objPoints):
            error = np.sum(np.abs(points_r - points_a))
            if error != 0:
                print(error)

        if (r_aruco_dict.bytesList != aruco_dict.bytesList).any():
            print('Aruco dictionary dont match')


if __name__=='__main__':

    folder = './aruco_markers'
    if not os.path.exists((folder)):
        os.makedirs(folder)
    # Board 1:
    nx = 4; ny = 3
    ids = []
    fig = plt.figure()
    aruco_dict_name = aruco.DICT_6X6_250
    aruco_dict = aruco.getPredefinedDictionary(aruco_dict_name)

    start = 1
    end = nx*ny+1
    for i in range(start, end):
        ax = fig.add_subplot(ny,nx, i)
        img = aruco.drawMarker(aruco_dict,i, 700)
        plt.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
        ids.append(i)

    #save map
    save(aruco_size=0.0439, fig=fig, ids=ids, filename= os.path.join(folder,"markers.png"), aruco_dict_name = aruco.DICT_6X6_250)


    #  board 2: 6 in one
    fig = plt.figure()
    nx = 3
    ny = 2
    start = end
    end = nx*ny + start
    ids = []
    for i in range(1 + start, nx*ny + 1 + start):
        ax = fig.add_subplot(ny,nx, i-start)
        img = aruco.drawMarker(aruco_dict,i, 700)
        plt.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
        ax.axis("off")
        ids.append(i)


    save(aruco_size=0.06728,
             fig=fig,
             ids = ids,
             filename= os.path.join(folder,"markers{}-{}.png".format(start,end)),
             aruco_dict_name = aruco.DICT_6X6_250)

    #board 3: 6 in one
    fig = plt.figure()
    nx = 3
    ny = 2
    start = end
    end = nx*ny + start
    ids = []
    for i in range(1 + start, nx*ny + 1 + start):
        ax = fig.add_subplot(ny,nx, i-start)
        img = aruco.drawMarker(aruco_dict,i, 700)
        plt.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
        ax.axis("off")
        ids.append(i)


    save(aruco_size=0.06728,
             fig=fig,
             ids = ids,
             filename= os.path.join(folder,"markers{}-{}.png".format(start,end)),
             aruco_dict_name = aruco.DICT_6X6_250)

    # print single aruco markers
    for i in range(1, nx*ny+1):
        fig = plt.figure()
        img = aruco.drawMarker(aruco_dict,i, 700)
        plt.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
        plt.axis('off')
        #save map
        save(aruco_size=0.0439,
             fig=fig,
             ids = [i],
             filename= os.path.join(folder,"markers_{}.png".format(i)),
             aruco_dict_name = aruco.DICT_6X6_250)


