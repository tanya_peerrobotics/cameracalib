# -*- coding: utf-8 -*-

import os
import numpy as np
from cv2 import aruco
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import PIL

from cameraCalibration import performCalibrationCheckerboard, performCalibrationAruco
from tally import getPoseAruco, getPoseArucoEach

from createArucoMMap import create_board

folder = '../data/dataset3'
mtx, dist, _, _ = performCalibrationCheckerboard(size_checkerboard=0.0245,
    folder = os.path.join(folder, 'checkerboard'))

boards = {}
boards['0-0.5'] = [create_board('./aruco_markers/markers.pdf.csv')]
boards['0.5-1'] = [create_board('./aruco_markers/markers1-5.pdf.csv'),
                   create_board('./aruco_markers/markers7-13.pdf.csv')]
boards['1-1.5'] = [create_board('./aruco_markers/markers1-5.pdf.csv'),
                   create_board('./aruco_markers/markers7-13.pdf.csv')]

for subFolder in ['0-0.5']: # '0.5-1', '1-1.5']:

    expFolder = os.path.join(folder, subFolder)
    outputFolder = os.path.join(expFolder, 'error_analysis')
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)

    errors = []
    errors_dict = {} # error accumulated errors across all images

    for (board, aruco_dict) in boards[subFolder]:

        # perform calibration
        act_mtx, act_dist, _, _ = performCalibrationAruco(folder = expFolder,
                                                          aruco_dict=aruco_dict,
                                                          board=board,
                                                          outputname = 'calibration_aruco')

        # experiment 1
        est_poses = getPoseAruco(folder = expFolder,
                                 aruco_dict=aruco_dict,
                                 board=board,
                                 mtx = mtx, #camera matrix
                                  dist =dist)

        act_poses = getPoseAruco(folder = expFolder,
                                 aruco_dict=aruco_dict,
                                 board=board,
                         mtx = act_mtx, #camera matrix
                         dist = act_dist,
                         outputname = 'calib_pose_aruco')



        for est_pose, act_pose in zip(est_poses, act_poses):

            _, est_rvec, est_tvec = est_pose
            _, act_rvec, act_tvec = act_pose

            error = np.abs(est_tvec - act_tvec)
            errors.append(error)

        # experiment 2
        markerLength = np.sqrt(np.sum(np.square(board.objPoints[0][0]-board.objPoints[0][1])))
        est_poses, est_id_list = getPoseArucoEach(folder = expFolder,
                                                  markerLength=markerLength,
                                                  aruco_dict=aruco_dict,
                                                  board=board,
                                                  mtx = mtx, #camera matrix
                                                  dist =dist)

        act_centers = [np.mean(points, axis = 0) for points in board.objPoints]
        act_ids = board.ids.reshape((-1,))


        for est_ids, est_poses_each in zip(est_id_list, est_poses):

            est_rvecs, est_tvecs, est_objPoints = est_poses_each

            for i in range(len(est_ids)):
                for j  in range(i, len(est_ids)):
                    tvec_i = est_tvecs[i]
                    tvec_j = est_tvecs[j]
                    if i != j:
                        # 2 ids considered are at index i and j

                        act_c_i = act_centers[np.argwhere( act_ids == est_ids[i])[0][0]]
                        act_c_j = act_centers[np.argwhere( act_ids == est_ids[j])[0][0]]

                        act_dist = round( np.sqrt(np.sum(np.square(act_c_i - act_c_j))),4)
                        est_dist = np.sqrt(np.sum(np.square(tvec_i - tvec_j)))

                        error = np.abs(act_dist - est_dist)
                        if act_dist in errors_dict:
                            errors_list = errors_dict[act_dist]
                            errors_list.append(error)
                            errors_dict[act_dist] = errors_list
                        else:
                            errors_dict[act_dist] = [error]


    #accumulate errors for experiment 1
    errors = np.asarray(errors).reshape((-1,3))
    mean_errors = np.mean(errors, axis = 0)

    errors_file = os.path.join(outputFolder, 'mean_error')
    hd = open(errors_file, 'w+')
    print('\n\nMean Errors: (in meters)\n', file = hd)
    print(mean_errors, file= hd)
    hd.close()

    # accumulate errors for experiment 2
    a_dict = errors_dict.copy()
    for error, elist in errors_dict.items():
        errors_dict[error] = np.mean(np.asarray(elist))

    df = pd.DataFrame()
    df['Act Distance'] = sorted(errors_dict)
    df['error(in m)'] = [ errors_dict[e] for e in sorted(errors_dict) ]

    errors_file = os.path.join(outputFolder, 'mean_error_each')
    df.to_csv(errors_file)





