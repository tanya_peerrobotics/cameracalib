#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 19:13:31 2020

@author: rm100
"""

#!/usr/bin/env python

import cv2
import numpy as np
import os
import glob
from cv2 import aruco


DEBUG = False
def performCalibrationCheckerboard(folder,
                                   CHECKERBOARD = (8,6),
                                   size_checkerboard = 0.0254, #in meters,
                                   extension = '*.png',
                                   outputname = 'calibration'):

    # output directory
    outputFolder = os.path.join(folder, outputname)
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)

    # Creating vector to store vectors of 3D points for each checkerboard image
    objpoints = []
    # Creating vector to store vectors of 2D points for each checkerboard image
    imgpoints = []

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    # Defining the world coordinates for 3D points
    objp = np.zeros((1, CHECKERBOARD[0] * CHECKERBOARD[1], 3), np.float32)
    objp[0,:,:2] = np.mgrid[0:CHECKERBOARD[0], 0:CHECKERBOARD[1]].T.reshape(-1, 2)

    # adding checkerboard size to world coordinates
    objp *= size_checkerboard

    # Extracting path of individual image stored in a given directory
    images = glob.glob(os.path.join(folder, extension))
    for fname in images:
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        # Find the chess board corners
        # If desired number of corners are found in the image then ret = true
        ret, corners = cv2.findChessboardCorners(gray,
                                                 CHECKERBOARD,
                                                 cv2.CALIB_CB_ADAPTIVE_THRESH +
                                                 cv2.CALIB_CB_FAST_CHECK +
                                                 cv2.CALIB_CB_NORMALIZE_IMAGE)

        """
        If desired number of corner are detected,
        we refine the pixel coordinates and display
        them on the images of checker board
        """
        if ret == True:
            objpoints.append(objp)
            # refining pixel coordinates for given 2d points.
            corners2 = cv2.cornerSubPix(gray, corners, (11,11),(-1,-1), criteria)

            imgpoints.append(corners2)

            # Draw and display the corners
            img = cv2.drawChessboardCorners(img, CHECKERBOARD, corners2, ret)

            if DEBUG == True:
                cv2.imshow('img',img)
                cv2.waitKey(0)

            else:
                filename = os.path.join(outputFolder,
                                        os.path.basename(fname))
                #print (filename)
                cv2.imwrite( filename, img)


    cv2.destroyAllWindows()

    h,w = img.shape[:2]

    """
    Performing camera calibration by
    passing the value of known 3D points (objpoints)
    and corresponding pixel coordinates of the
    detected corners (imgpoints)
    """
    # ret, mtx, dist, rvecs, tvecs,  = cv2.calibrateCamera(objpoints,
    #                                                      imgpoints,
    #                                                      gray.shape[::-1],
    #                                                      None, None)

    ret, mtx, dist, rvecs, tvecs, stdDeviationsIntrinsics, stdDeviationsExtrinsics, perViewErrors = cv2.calibrateCameraExtended(objpoints, imgpoints, gray.shape[::-1], None, None)

    calibrationFile = os.path.join(outputFolder, 'calib.txt')
    hd = open(calibrationFile, 'w')

    np_tvecs = []
    for t in tvecs:
        np_tvecs.append(t.reshape((3,)))
    tvecs = np.asarray(np_tvecs)

    np_rvecs = []
    for r in rvecs:
        np_rvecs.append(r.reshape((3,)))
    rvecs = np.asarray(np_rvecs)


    print("Camera matrix : \n", file = hd)
    print(mtx, file = hd)
    print("\n\ndist : \n", file = hd)
    print(dist, file = hd)
    print("\n\nrvecs : \n", file = hd)
    print(rvecs, file = hd)
    print("\n\ntvecs : \n", file = hd)
    print(tvecs, file = hd)
    print ('\n\n stdDeviationsIntrinsics : \n', file = hd)
    print(stdDeviationsIntrinsics, file = hd)
    print('\n\nStdDeviationsExtrinsics : \n', file = hd)
    print(stdDeviationsExtrinsics, file = hd)
    print('\n\nPerViewError: \n', file = hd)
    print(perViewErrors, file = hd)

    #import pdb; pdb.set_trace()
    hd.close()

    return mtx, dist, rvecs, tvecs


def performCalibrationAruco(folder,
                            aruco_dict=None, # bu default map will be taken
                            board=None,
                            # markerLength = 0.0273, #in meters,
                            # markerSeparation=0.0055, # in meters
                            extension = '*.png',
                            outputname = 'calibration'):
    # output directory
    outputFolder = os.path.join(folder, outputname)
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)

    # same as used for generating aruco markers
    # if aruco_dict is None:
    #     aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_6X6_250)

    # # get board or placement of markers in the map
    # if board is None:
    #     board = aruco.GridBoard_create(4,
    #                                    3,
    #                                    markerLength,
    #                                    markerSeparation,
    #                                    aruco_dict,
    #                                    1)

    arucoParams = aruco.DetectorParameters_create()

    images = glob.glob(os.path.join(folder, extension))
    res = cv2.cvtColor(cv2.imread(images[0]),cv2.COLOR_BGR2GRAY).shape

    #preparing lists for calibrating camera
    numMarkers, corners_list, id_list = [], [], []

    for imageId, fname in enumerate(images):
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        corners, ids, rejectedImgPoints = aruco.detectMarkers(gray,
                                                              aruco_dict,
                                                              parameters=arucoParams)


        img	= aruco.drawDetectedMarkers(img, corners, ids)

        if DEBUG == True:
            cv2.imshow('img',img)
            cv2.waitKey(0)

        else:
            filename = os.path.join(outputFolder,
                                    os.path.basename(fname))
            #print (filename)
            cv2.imwrite( filename, img)

        if len(corners_list) == 0:
            corners_list = corners
            id_list = ids


        else:
            corners_list = np.vstack((corners_list, corners))
            id_list = np.vstack((id_list,ids))

        numMarkers.append(len(ids))

    numMarkers = np.asarray(numMarkers)
    ret, mtx, dist, rvecs, tvecs, stdDeviationsIntrinsics, stdDeviationsExtrinsics, perViewErrors = aruco.calibrateCameraArucoExtended(corners_list,
                                                          id_list,
                                                          numMarkers,
                                                          board,
                                                          res,
                                                          None,
                                                          None)

    np_tvecs = []
    for t in tvecs:
        np_tvecs.append(t.reshape((3,)))
    tvecs = np.asarray(np_tvecs)

    np_rvecs = []
    for r in rvecs:
        np_rvecs.append(r.reshape((3,)))
    rvecs = np.asarray(np_rvecs)

    calibrationFile = os.path.join(outputFolder, 'calib.txt')
    hd = open(calibrationFile, 'w+')

    print("Camera matrix : \n", file = hd)
    print(mtx, file = hd)
    print("\n\ndist : \n", file = hd)
    print(dist, file = hd)
    print("\n\nrvecs : \n", file = hd)
    print(rvecs, file = hd)
    print("\n\ntvecs : \n", file = hd)
    print(tvecs, file = hd)
    print ('\n\n stdDeviationsIntrinsics : \n', file = hd)
    print(stdDeviationsIntrinsics, file = hd)
    print('\n\nStdDeviationsExtrinsics : \n', file = hd)
    print(stdDeviationsExtrinsics, file = hd)
    print('\n\nPerViewError: \n', file = hd)
    print(perViewErrors, file = hd)

    #import pdb; pdb.set_trace()
    hd.close()

    return mtx, dist, rvecs, tvecs

