#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 22:42:21 2020

@author: rm100
"""

import numpy as np
import cv2, PIL
from cv2 import aruco
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import glob
import os

DEBUG= False

def getPoseAruco(folder,
                 mtx, #camera matrix
                 dist, #distortion parameters
                 aruco_dict=None, # bu default map will be taken
                 board=None,
                 # markerLength = 0.0273, #in meters
                 # markerSeparation=0.0055,
                 extension='*.png',
                 outputname = 'calib_pose'):

    # output directory
    outputFolder = os.path.join(folder, outputname)
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)

    # same as used for generating aruco markers
    # if aruco_dict is None:
    #     aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_6X6_250)

    # # get board or placement of markers in the map
    # if board is None:
    #     board = aruco.GridBoard_create(4,
    #                                    3,
    #                                    markerLength,
    #                                    markerSeparation,
    #                                    aruco_dict,
    #                                    1)

    arucoParams = aruco.DetectorParameters_create()

    images = glob.glob(os.path.join(folder, extension))
    estimatedPoseList = []

    for imageId, fname in enumerate(images):
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        corners, ids, rejectedImgPoints = aruco.detectMarkers(gray,
                                                              aruco_dict,
                                                              parameters=arucoParams)

        estimatedPose = aruco.estimatePoseBoard(corners,
                                                ids,
                                                board,
                                                mtx,
                                                dist,
                                                None,
                                                None)
        ret, rvec, tvec = estimatedPose
        #import pdb;pdb.set_trace()

        img	= aruco.drawDetectedMarkers(img, corners, ids)
        img	= aruco.drawAxis(img,
                             mtx,
                             dist,
                             rvec=rvec,
                             tvec=tvec,
                             length=0.1)

        if DEBUG == True:
            cv2.imshow('img',img)
            cv2.waitKey(0)

        else:
            filename = os.path.join(outputFolder,
                                    os.path.basename(fname))
            #print (filename)
            cv2.imwrite( filename, img)

        estimatedPoseList.append(estimatedPose)

    return estimatedPoseList


def getPoseArucoEach(folder,
                 mtx, #camera matrix
                 dist, #distortion parameters
                 markerLength,
                 aruco_dict=None, # bu default map will be taken
                 board=None,
                 # markerLength = 0.0273, #in meters
                 # markerSeparation=0.0055, #in meters
                 extension='*.png',
                 outputname = 'calib_pose_single'):

    # output directory
    outputFolder = os.path.join(folder, outputname)
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)

    # same as used for generating aruco markers
    # if aruco_dict is None:
    #     aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_6X6_250)

    # # get board or placement of markers in the map
    # if board is None:
    #     board = aruco.GridBoard_create(4,
    #                                    3,
    #                                    markerLength,
    #                                    markerSeparation,
    #                                    aruco_dict,
    #                                    1)

    arucoParams = aruco.DetectorParameters_create()

    images = glob.glob(os.path.join(folder, extension))
    estIntPosesList = []
    id_list = []

    for imageId, fname in enumerate(images):
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        corners, ids, rejectedImgPoints = aruco.detectMarkers(gray,
                                                              aruco_dict,
                                                              parameters=arucoParams)

        estIndposes = aruco.estimatePoseSingleMarkers(corners,
                                                  markerLength,
                                                  mtx,
                                                  dist)

        img	= aruco.drawDetectedMarkers(img, corners, ids)
        rvecs, tvecs, objPoints = estIndposes
        for rvec, tvec in zip(rvecs, tvecs):
            img	= aruco.drawAxis(img,
                                 mtx,
                                 dist,
                                 rvec=rvec,
                                 tvec=tvec,
                                 length=0.1)

        if DEBUG == True:
            cv2.imshow('img',img)
            cv2.waitKey(0)

        else:
            filename = os.path.join(outputFolder,
                                    os.path.basename(fname))
            #print (filename)
            cv2.imwrite( filename, img)

        estIntPosesList.append(estIndposes)
        id_list.append(ids)

    return estIntPosesList, id_list





# error = None

# for i in range(len(images)):
#     e_tvecs = estimatedPoseList[i][2]
#     a_tvecs = tvecs[i]

#     if error is None:
#         error = np.abs(e_tvecs - a_tvecs)
#     else:
#         error += np.abs(e_tvecs - a_tvecs)

# avg_error = error/len(images)
# print(avg_error)

# rotation_mat = np.zeros(shape=(3, 3))
# R = cv2.Rodrigues(rvecs[0], rotation_mat)[0]
# P = np.column_stack((np.matmul(mtx,R), tvecs[0]))

# # reprojection errors

# tot_error = 0
# objpoints = np.asarray(board.objPoints)
# ids = board.ids

# for i in range(len(imageIdList)): # num of images
#     imageId = imageIdList[i]
#     actId = id_list[i]
#     actPoints = corners_list[i]

#     w_ind = np.argwhere(board.ids == actId)[0][0]
#     pointProjectns, _ = cv2.projectPoints(objpoints[w_ind],
#                                           rvecs[i],
#                                           tvecs[i],
#                                           mtx,
#                                           dist)
#     # import pdb;pdb.set_trace()

#     A = actPoints.reshape((-1,2))
#     P = pointProjectns.reshape((-1,2))

#     #error = cv2.norm(A,P,cv2.NORM_L2)/len(pointProjectns)


# def compute_distances(tvecs, ids):
#     try:
#         arg1=np.argwhere(ids == 1)[0][0]
#         tvec1 = tvecs[arg1]
#         arg2= np.argwhere(ids == 4)[0][0]
#         tvec2 = tvecs[arg2]
#         dist = np.sqrt(np.sum(np.square(tvec1-tvec2)))
#         #print(dist)
#         return dist
#     except:
#         import pdb; pdb.set_trace
#         print(ids)
#         return None

# def compute_angles(rvecs, ids):
#     try:
#         arg1=np.argwhere(ids == 1)[0][0]
#         rvec1 = rvecs[arg1]
#         arg2= np.argwhere(ids == 5)[0][0]
#         rvec2 = rvecs[arg2]
#         rot = np.abs(rvec1-rvec2)
#         rot[rot > 3.14] -= 6.28
#         rot = np.abs(rot)

#         print(rot)
#         return rot
#     except:
#         import pdb; pdb.set_trace
#         print(ids)
#         return None



# # aruco.estimatePoseBoard()
# rots = []
# for i in range(len(images)):
#     rvecs = estIntPosesList[i][0]
#     ids = idList[i]
#     rots.append(np.mean(compute_angles(rvecs, ids)))

# print(np.mean(rots))


# distances = []

# for i in range(len(images)):
#     tvecs = estIntPosesList[i][1]
#     ids = idList[i]
#     d = compute_distances(tvecs, ids)
#     if d is not None:
#         distances.append(d)

# print(np.mean(distances))






