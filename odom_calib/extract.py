# -*- coding: utf-8 -*-
import numpy as np
import rosbag, time
import csv
import glob
import os
import cv2
import pandas as pd

from sensor_msgs.msg import Image
from cv_bridge import CvBridge

folder = '/home/tanya/work/cameracalib/catkin_ws/rot'
extension = '*.bag'
bagFiles = glob.glob(os.path.join(folder, extension))
NUM = 20

bridge = CvBridge()

for bagFile in bagFiles:
    bag=rosbag.Bag(bagFile)
    output_dir  = bagFile.split('.bag')[0]
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)


    #get total number of images in the folder
    num_images = 0
    #get number of images in the set
    for topic, msg, t in bag.read_messages(topics=['/camera/color/image_raw']):
        num_images += 1
    stepSize = num_images/NUM

    imgctr = 0
    ticks_r_list = []; ticks_r=0
    ticks_l_list = []; ticks_l=0
    imagename_list = []

    for topic, msg, t in bag.read_messages(topics=['/lwheel',
                                                   '/rwheel',
                                                   '/camera/color/image_raw']):
        #ticks_r.append(msg.data)
        if topic == '/lwheel':
            ticks_l += msg.data
        if topic == '/rwheel':
            ticks_r += msg.data
        if topic == '/camera/color/image_raw':
            imgctr += 1
            if imgctr%stepSize == 0:
                cv_img = bridge.imgmsg_to_cv2(msg, desired_encoding="passthrough")
                imagename= os.path.join(output_dir, "frame%06i.png" % imgctr)
                cv2.imwrite(imagename, cv_img)

                rot_r = (float(ticks_r) / 600.0)*360.0
                rot_l = (float(ticks_l) / 600.0)*360.0

                imagename_list.append(imagename)
                ticks_l_list.append(ticks_l)
                ticks_r_list.append(ticks_r)

    # dump ticks and images to a csv
    df = pd.DataFrame()
    df['lwheel'] = ticks_l_list
    df['rwheel'] = ticks_r_list
    df['filename'] = imagename_list

    csvfile = os.path.join(output_dir, 'data.csv')
    df.to_csv(csvfile, sep=';')


