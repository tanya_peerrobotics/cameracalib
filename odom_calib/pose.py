# -*- coding: utf-8 -*-
import numpy as np
import glob
import os
import cv2
import pandas as pd

from calibration_scripts.createArucoMMap import create_board
from calibration_scripts.cameraCalibration import performCalibrationCheckerboard
from calibration_scripts.tally import getPoseAruco


folder = '/home/tanya/work/cameracalib/catkin_ws/rot'
extension = '*.png'
bagfiles = glob.glob(os.path.join(folder, '*.bag'))
folders = [bagfile.split('.bag')[0] for bagfile in bagfiles]


for folder  in folders:
    images = glob.glob(os.path.join(folder, '*.png'))
    datafile = os.path.join(folder, 'data.csv')
    df = pd.read_csv(datafile, sep=';')
    eimages = df['filename'].values

    # filtered version
    mask = [False]*len(df)

    for i, file in enumerate(eimages):
        if file in images:
            mask[i] = True

    df= df[mask]
    eimages = df['filename'].values

    # load board for this image set
    boardfile = glob.glob(os.path.join(folder, '*.png.csv'))[0]
    board, aruco_dict = create_board(boardfile)

    # camera calibration TODO: replace with reading from file
    calibfolder = '../data/dataset3'
    mtx, dist, _, _ = performCalibrationCheckerboard(size_checkerboard=0.0245,
    folder = os.path.join(calibfolder , 'checkerboard'))

    #get pose for these images
    poses, rel_images = getPoseAruco(images=eimages,
                         aruco_dict=aruco_dict,
                         board=board,
                         mtx = mtx, #camera matrix
                         dist = dist,
                         outputname = 'calib_pose_aruco',
                         return_filenames = True)


    #mask the dataframe according to it.
    # filtered version
    mask = [False]*len(df)

    for i, file in enumerate(eimages):
        if file in rel_images:
            mask[i] = True

    df= df[mask]

    # combine these in single array
    rvecs = []
    tvecs = []
    for _, rvec, tvec in poses:
        rvecs.append(rvec.reshape((3,)))
        tvecs.append(tvec.reshape((3,)))
    rvecs = np.asarray(rvecs)
    tvecs = np.asarray(tvecs)

    #calculate angles
    angles_o = np.arctan(tvecs[:,0]/tvecs[:,2])


    #dump these poses and rticks in file
    ndf = pd.DataFrame()
    ndf['rwheel'] = df['rwheel']
    ndf['lwheel'] = df['lwheel']
    ndf['angles'] = angles_o
    for i in range(0,3):
        ndf['rvec_' + str(i)] = rvecs[:,i]
        ndf['tvec_' + str(i)] = tvecs[:,i]

    f = os.path.join(folder, 'results')
    if not os.path.exists(f):
        os.makedirs(f)
    ndf.to_csv(os.path.join(f, 'poses.csv'), sep = ';')














