# README #

This README would normally document whatever steps are necessary to get your application up and running.

For calibration of mobile robotic arm, the following steps are performed:
* Camera calibration
* Robot Manipulator calibration (eye-in-hand) config
* Mobile Base calibration.

The structure of this repository is such:
* Calibration_scripts: It list all the scripts for calibrating the camera and running error analysis on each of the collected sets.
* DH Param Scripts: Scripts to generate DH parameters for robotic arm.
* odom_calib: scripts for collecting data and running odometry calibration.