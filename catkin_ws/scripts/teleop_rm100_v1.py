import os
import libtmux
import time
from datetime import datetime

class runRobomuse:
	def __init__(self, sName = 'pr'):
		self.sessionName = sName
		self.runRoscore = True
		self.server = libtmux.Server()
		#kill_session()
		self.session = self.server.new_session(self.sessionName, attach=False)
		self.window = self.session.new_window(attach=True, window_name="runRoboMuse")
		self.paneDict = {}
		self.bInit = False
		self.windowTeleOp = None
		
	def logData(self, data):
		for log in data:
			print log	

	def RobomuseInit(self):
		self.bInit = True
		pane = self.window.list_panes()[0];
		self.paneDict['roscore'] = pane
		pane.send_keys(cmd='source install_isolated/setup.bash', enter=True)
		pane.send_keys(cmd='roscore')
		time.sleep(4)		
		roslog = pane.capture_pane()
		print roslog		

		pane = pane.split_window(attach=True,vertical=True)
		self.paneDict['Motors'] = pane
		pane.send_keys(cmd='source install_isolated/setup.bash', enter=True)
		pane.send_keys(cmd='chmod a+rw /dev/ttyACM0')
		pane.send_keys(cmd='rosrun rosserial_python serial_node_kangaroo.py /dev/ttyACM0 _baud:=38400')
		time.sleep(2)
		rosSerial = pane.capture_pane()
		print rosSerial

		pane = pane.split_window(attach=True,vertical=True)
		self.paneDict['Teleop'] = pane
		pane.send_keys(cmd='source install_isolated/setup.bash', enter=True)
		pane.send_keys(cmd='rosrun robot_hands-on teleop.py')
		time.sleep(2)
		rosSerial = pane.capture_pane()
		print rosSerial		
				
		pane = pane.split_window(attach=True,vertical=True)
		self.paneDict['Camera'] = pane
		pane.send_keys(cmd='source install_isolated/setup.bash', enter=True)
		pane.send_keys(cmd='roslaunch realsense2_camera rs_camera.launch')
		time.sleep(2)
		rosSerial = pane.capture_pane()
		print rosSerial

if __name__ == '__main__':
	r = runRobomuse('pr')
	r.RobomuseInit()
	#r.useTeleOp()
