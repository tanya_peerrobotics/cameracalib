import os
import libtmux
import time
from datetime import datetime

class runRobomuse:
	def __init__(self, sName = 'pr'):
		self.sessionName = sName
		self.runRoscore = True
		self.server = libtmux.Server()
		#kill_session()
		self.session = self.server.new_session(self.sessionName, attach=False)
		self.window = self.session.new_window(attach=True, window_name="runRoboMuse")
		self.paneDict = {}
		self.bInit = False
		self.windowTeleOp = None
		
	def logData(self, data):
		for log in data:
			print log	

	def RobomuseInit(self):
		self.bInit = True
		pane = self.window.list_panes()[0];
		self.paneDict['roscore'] = pane
		pane.send_keys(cmd='source /opt/ros/kinetic/setup.bash', enter=True)
		pane.send_keys(cmd='source devel/setup.bash', enter=True)
		pane.send_keys(cmd='roscore')
		time.sleep(4)		
		roslog = pane.capture_pane()
		print roslog		

		pane = pane.split_window(attach=True,vertical=True)
		self.paneDict['Motors'] = pane
		pane.send_keys(cmd='source /opt/ros/kinetic/setup.bash', enter=True)
		pane.send_keys(cmd='source devel/setup.bash', enter=True)
		pane.send_keys(cmd='rosrun pylon_camera pylon_camera_node')
		time.sleep(5)
		rosSerial = pane.capture_pane()
		print rosSerial


if __name__ == '__main__':
	r = runRobomuse('pr')
	r.RobomuseInit()
	#r.useTeleOp()
