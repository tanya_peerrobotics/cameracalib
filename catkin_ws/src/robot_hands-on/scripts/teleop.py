#!/usr/bin/env python

# Copyright (c) 2011, Willow Garage, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the Willow Garage, Inc. nor the names of its
#      contributors may be used to endorse or promote products derived from
#       this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import rospy
from std_msgs.msg import Float32
import sys, select, os
if os.name == 'nt':
  import msvcrt
else:
  import tty, termios

msg = """
Control Your Robomuse5!
---------------------------
Moving around:
        w
   a    s    d
        x
w/x : increase/decrease linear velocity (Robomuse : ~ 0.22)
a/d : increase/decrease angular velocity (Robomuse : ~ 2.84)
space key, s : force stop
CTRL-C to quit
"""

e = """
Communications Failed
"""

def getKey():
    if os.name == 'nt':
      return msvcrt.getch()

    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key


if __name__=="__main__":
    if os.name != 'nt':
        settings = termios.tcgetattr(sys.stdin)

    rospy.init_node('robomuse5_teleop')
    rpub = rospy.Publisher('/rmotor_cmd', Float32, queue_size=10)
    lpub = rospy.Publisher('/lmotor_cmd', Float32, queue_size=10)
    
    rate = rospy.Rate(10)
    status = 0

    try:
        print msg
        while(1):
            key = getKey()
            if key == 'w' :
            	rpub.publish(0.5)
                lpub.publish(0.5)
            elif key == 'x' :
            	rpub.publish(-0.5)
                lpub.publish(-0.5)
                
            elif key == 'a' :
            	rpub.publish(0.5)
                lpub.publish(-0.5)
            elif key == 'd' :
            	rpub.publish(-0.5)
                lpub.publish(0.5)
            elif key == ' ' or key == 's' :
            	rpub.publish(0.0)
                lpub.publish(0.0)
            else:
                if (key == '\x03'):
                    break

            if status == 20 :
                print msg
                status = 0
            rate.sleep()


    except:
        print e

    finally:
        rpub.publish(0.0)
        lpub.publish(0.0)

    if os.name != 'nt':
       termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
